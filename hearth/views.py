# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.template import Context, loader
from django.shortcuts import redirect
from django.views.generic.list import ListView
import random
import collections


def challenges():
    baselist = [
        'You can only play cards that cost ' + str(random.randint(2,7)) + ' or more.',
        'You can only play cards that cost ' + str(random.randint(1,7)) + ' or less.',
        'You can only play one card each turn.',
        'You must play at least a card every turn.',
        'If your deck is out of cards, you win.',
        'You cannot use your hero power this game.',
        'Type \'alex\' on the search box, use only cards from that search.',
        'Type \'damage\' on the search box, use only cards from that search.',
        'Type \'random\' on the search box, use only cards from that search.',
        'Type \'as\' on the search box, use only cards from that search.',
        'Type \'spell\' on the search box, use only cards from that search.',
        'Type \'turn\' on the search box, use only cards from that search.',
        'Type \'attack\' on the search box, use only cards from that search.',
        'Type \'whenever\' on the search box, use only cards from that search.',
        'Type \'battlecry\' on the search box, use only cards from that search.',
        'Type \'taunt\' on the search box, use only cards from that search.',
        'Type \'u\' on the search box, use only cards from that search.',
        'Type \'summon\' on the search box, use only cards from that search.',
        'Type \'+1\' on the search box, use only cards from that search.',
        'Type \'her\' on the search box, use only cards from that search.',
        'You can only play cards that cost between ' + str(random.randint(1,4)) + ' and ' + str(random.randint(5,10)),
        'Pick Mage class, you must use your hero power on yourself every turn if it\'s possible.',
        'Pick Priest class, you must use your hero power on yourself every turn if it\'s possible.',
        'Pick Paladin class, you must use your hero power every turn if it\'s possible.',
        'Skip the first ' + str(random.randint(2,5)) + ' turns',
        'You can only hit your oponent with your minions if you can kill him this turn.',
        'You can\'t use damaging spells.',
        'Your minions must have ' + str(random.randint(3,5)) + ' or more attack.',
        'Your minions must have ' + str(random.randint(3,5)) + ' or more health.',
        'Your minions must have higher base health than attack.',
        'Your minions must have equal or higher attack than their mana cost.',
        'Your minions must have equal base health and attack.',
        'You must play at least one card of every mana cost up to 9.',
        'You must always attack and target minions before the enemy hero.',
        'You must always attack and target the enemy hero if possible (Taunts and minion damage spells do not apply).',
        'If you put the enemy hero at 1 health you win, if you kill him on your turn you lose. (Dying on your own turn means losing)',
        'You must always attack and target minions before the enemy hero.',
        'Whoever dies first, wins.',
        'Open a new deck and close it without adding any cards, let the Innkeeper automatically fill the deck for you.',
        'Your spells can only target your own minions.',
	'Your deck can\'t have any spells.',
	'You can not use any Rare, Epic or Legendary card of any set.',
	'Your deck can\'t have any minions.',
	'The art of your cards must have at least one male.',
	'The art of your cards must have at least one female.',
	'The art of your cards must have a non-humanoid character.',
	'Your minions must belong to a tribe (Murloc, Dragon, Mech, Demon, Beast or Pirate).',
	'Only minions with buffs can attack.',
	'Choose the first 30 non-class cards of your collection.',
	'Choose the last 30 non-class cards of your collection.',
	'Choose the 30 most expensive non-class cards of your collection. (Search for Legendary, Epic, Rare)',
	'You can only play class cards.',
	'If you can play the card you just drew, you must play it.',
	'You can only play cards with even mana cost (0,2,4,6,8,10.)',
	'You can only play cards with uneven mana cost (1,3,5,7,9).',
	'You can only choose cards from the basic set (Filter by Basic).',
	'You can only play cards that cost EXACTLY 2.',
	'Your can only play cards with card text of 5 words or less.',
	'Choose 10 cards for your deck, let the innkeeper fill the rest.',
	'Use the deck helper function to draft your deck.',
	'If you get a full board, you win. You can\'t directly kill your oponent nor play Onyxia.',
	'Your minions MUST ATTACK every turn.',
	'Your minions can\'t attack.',
	'You can\'t play any cards or use your hero power until you\'re at 10 mana crystals (You may use the coin).',
	'When attacking, the minion with the lowest attack value must attack before the rest each time.',
	'When attacking, the minion with the highest attack value must attack before the rest each time.',
	'You must kill your oponent using your hero.',
	'You will always play your leftmost card if you can.',
	'Only taunt minions can attack.',
	'The total mana cost of your deck must not exceed ' + str(random.randint(50,150)),
	'If your enemy has no minions on his board, you can\'t kill him',
	'Roll until you get 3 compatible rules, use them',
	'Only minions with buffs of any kind can attack',
	'You cannot play a minion that shares the same attack with another on the board',
	'You can\'t play cards that can affect multiple targets',
	'If you have 3 minions and their attacks add up to 7 total, you win',
	'Whenever you have an odd number of mana crystals, you cannot play anything',
	'Minions without card text cannot be targeted by spells or hero powers',
    ]

    return [random.choice(baselist),len(baselist)]


def index(request):
    challenge = challenges()
    return render_to_response('hearth/index.html',{'challenge':challenge[0], 'amount':challenge[1]}, context_instance=RequestContext(request))
